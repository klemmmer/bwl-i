__author__ = 'Fabian Ball'


class MetaAgent(type):
    def __new__(mcs, name, bases, attrs):
        # Collect all sensor names
        sensors = {}
        for attr_name in attrs:
            if attr_name.endswith('_sensor'):
                property_name = attr_name.replace('_sensor', '')
                sensors[property_name] = None

        attrs['_sensors'] = sensors
        for base in bases:
            attrs['_sensors'].update(getattr(base, '_sensors', {}))

        return super(MetaAgent, mcs).__new__(mcs, name, bases, attrs)

    def __call__(cls, *args, **kwargs):
        instance = super(MetaAgent, cls).__call__(*args, **kwargs)
        # Set the bound sensor methods
        for name in instance._sensors:
            instance._sensors[name] = getattr(instance, name + '_sensor')

        return instance


class Agent(object):
    __metaclass__ = MetaAgent

    def __init__(self):
        self._sensor_data = {}

    @property
    def sensor_data(self):
        return self._sensor_data

    def sense(self, environment):
        """
        Query all sensors and return the results.
        :param environment:
        :return:
        """
        return {name: sensor(environment) for name, sensor in self._sensors.iteritems()}

    def act(self, environment):
        self._sensor_data = self.sense(environment)

    def _default_sensor_(self, name, env):
        return getattr(env, name)


class StockTradingAgent(Agent):

    def __init__(self, budget):
        super(StockTradingAgent, self).__init__()
        self._budget = float(budget)
        self._stock_amount = 0

    @property
    def budget(self):
        """
        Return the current available budget

        :return:
        """
        return self._budget

    @property
    def price(self):
        """
        Return the latest known stock price that was queried by the sensor.
        :return:
        """
        return self.sensor_data.get('price')

    @property
    def stock_amount(self):
        """
        Return the current stock amount.
        :return:
        """
        return self._stock_amount

    @property
    def current_stock_value(self):
        """
        The value of the stocks.
        :return:
        """
        return self.price * self.stock_amount if self.price else 0

    @property
    def current_total_value(self):
        return self.current_stock_value + self.budget

    def price_sensor(self, environment):
        """
        A sensor that gets the price from the environment.
        :param environment:
        :return:
        """
        return self._default_sensor_('price', environment)

    def __str__(self):
        return 'Budget: {:.2f}, Stock options: {} (Value: {:.2f}), Price: {:.2f}'.format(self.budget,
                                                                                         self.stock_amount,
                                                                                         self.current_stock_value,
                                                                                         self.price or 0)

    def result(self):
        """
        Return a string that shows the overall value, i.e. sum of budget and stock value
        :return:
        """
        tpl = """
**Sensor data**
Price: {price}
**Agent state**
Budget: {budget:.2f}, Stock amount: {stock}, Stock value={value:.2f}
=> Total value={total}
        """
        return tpl.format(budget=self.budget, stock=self.stock_amount, value=self.current_stock_value,
                          total=self.current_total_value, price=self.price)

    def buy(self, amount):
        """
        Buy *amount* stocks, but no more than possible given the budget.

        :param amount:
        :return: The actual amount bought
        """
        amount = min(amount, int(self.budget/self.price))

        self._stock_amount += amount
        self._budget -= amount * self.price
        assert self.budget >= 0

        return amount

    def sell(self, amount):
        """
        Sell *amount* stocks but no more than currently owned.
        :param amount:
        :return: The actual amount sold
        """
        amount = min(amount, self.stock_amount)

        self._stock_amount -= amount
        self._budget += amount * self.price
        assert self.stock_amount >= 0

        return amount

    def act(self, environment):
        """
        Let the agent act:
        - Query the sensors
        - Choose an action
        - Possibly modify the environment through its interface

        :param environment: The current environment
        """
        super(StockTradingAgent, self).act(environment)
