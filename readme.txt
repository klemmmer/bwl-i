ALLGEMEINES:
Dies ist eine sehr simple Umsetzung einer Umgebung (environment.py) mit der ein Agent (agent.py) interagieren soll.
Die Umgebung ist ein sehr simpler Aktienmarkt auf dem eine Aktie zum aktuellen Preis ge- oder verkauft werden kann 
(keine Gebühren/Steuern/... o.Ä.). Der Trading-Agent hat ein Startbudget, das er zum Kaufen verwenden kann.

AUFGABE:
Implementieren Sie die 'act' Methode des Agenten. Benennen Sie dazu die Datei
'Agent_Name1_Name2.py' im Ordner 'agents' entsprechend Ihren Nachnamen um (lexikographisch geordnet)
und tragen Sie Ihre Namen in die Liste 'names' in der Agenten-Klasse (class StudentStockTradingAgent) ein.
In der Klasse finden Sie weitere Hinweise zur Implementierung als Kommentare.

Die mitgelieferten Daten (data.csv) werden zum Simulieren benutzt. Halten Sie Ihre Implementierung aber allgemein,
für die Bewertung wird ein anderer Datensatz verwendet.

ABGABE:
Geben Sie NUR Ihre Agenten-Klasse ab (d.h. 'Agent_Name1_Name2.py') und ändern Sie keinen Code an anderen Stellen
(zum Testen etc. natürlich gerne).

BEWERTUNG:
In erster Linie geht es um Softwareagenten und Entscheidungstheorie, wie ein simpler Agent implementiert werden könnte
und nicht um High-Performance-Trading.
D.h. Sie dürfen sich natürlich gerne kreativ auslassen und Ihren Agenten immer weiter optimieren, allerdings fließt
die eigentliche Trading-Performance nicht in die Bewertung ein (außer der Agent "verstößt" gegen das in der Vorlesung gelernte).

TECHNISCHES:
Das Simulationsprogramm ist in Python (Version 2.7.x) [1] geschrieben. Um es auszuführen, wird Python in der aktuellsten 2.7er
Version benötigt. Weitere Bibliotheken werden nicht benötigt.
Das Programm wird ausgeführt mit:
"python runner.py"

"python runner.py -h" zeigt bereitgestellte Programmparameter an, welche ggf. verändert werden können.

Die zu implementierende 'act'-Methode befindet sich in der Klasse Agent in der gleichnamigen Datei. Änderungen in
anderen Programmteilen müssen/sollen nicht vorgenommen werden.

FRAGEN:
Sollten Sie Probleme haben oder einen Fehler entdecken, wenden Sie sich bitte per Mail an den Übungsleiter (ball@kit.edu).


[1] https://www.python.org/

Tutorials/Dokumentation:
[2] https://docs.python.org/2/tutorial/index.html
[3] http://learnpythonthehardway.org/book/
