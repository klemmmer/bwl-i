import abc


class Environment(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def update(self):
        pass


class SimulatedEnvironment(Environment):
    pass


class StockTradingEnvironment(SimulatedEnvironment):
    def __init__(self, data):
        self._data = data
        self._data_iter = iter(data)

        self._current_data = None
        self.update()

    def update(self):
        self._current_data = next(self._data_iter)

    @property
    def price(self):
        mean_price = sum(self._current_data[k] for k in ('Open', 'Close', 'Low', 'High')) / 4
        return mean_price

    def __iter__(self):
        return self

    def next(self):
        """
        Implement iterator interface but return a self ref.
        The environment is updated.
        :return:
        """
        self.update()
        return self
