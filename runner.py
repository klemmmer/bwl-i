import argparse
import csv
import datetime

from agents import get_agents
from environment import StockTradingEnvironment


def _get_argparser():
    argp = argparse.ArgumentParser()
    argp.add_argument('-b',
                      '--budget',
                      type=int,
                      help="The budget in the beginning",
                      default=1000)
    argp.add_argument('-d',
                      '--data',
                      type=argparse.FileType('r'),
                      help="Path to a .csv data file",
                      default='data.csv')
    # argp.add_argument('-i',
    #                   '--infinite',
    #                   type=bool,
    #                   default=False,
    #                   nargs='?')

    return argp


def _parse_data(handle):
    with handle:
        reader = csv.DictReader(handle, delimiter=',')
        return sorted([_parse_line(line) for line in reader], key=lambda l: l['Date'])


def _parse_line(line):
    for k in line:
        if k == 'Date':
            line[k] = datetime.datetime.strptime(line[k], '%Y-%m-%d').date()
        else:
            line[k] = float(line[k])

    return line


def run(agent, environment):
    for t, env in enumerate(environment):
        agent.act(env)

        print 'Simulation round {}'.format(t)
        print agent.result()

    return agent


if __name__ == "__main__":
    argp = _get_argparser()
    args = argp.parse_args()

    agents = get_agents()
    data = _parse_data(args.data)
    budget = args.budget

    if len(agents) == 1:
        print 'Running agent of {}'.format(' and '.join(agents[0].names))
        agent = run(agents[0](budget), StockTradingEnvironment(data))
    elif len(agents) > 1:
        print 'Found more than one agent. Exiting.'
