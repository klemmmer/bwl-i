# -*- coding: utf-8 -*-
from agent import StockTradingAgent
import math

# PLEASE RENAME THIS FILE ACCORDING TO YOUR LASTNAMES! USE LEXICOGRAPHIC ORDERING


class StudentStockTradingAgent(StockTradingAgent):

    """
    Implement your simplified stock trading agent here.
    The __init__ method initializes the agent instance. If you want to implement e.g. a memory (or goals) for
    your agent, do it there.
    """

    names = ['Burot, Jean-Luc', 'Harnischmacher, Clemens']  # PLEASE ENTER YOUR NAMES HERE

    def __init__(self, budget):
        super(StudentStockTradingAgent, self).__init__(budget)

        # Insert custom code here if needed, e.g.:
        self.memory = []
        # creates an empty list that can be used as memory or e.g.:

        # Remember the price of the previous period.
        self.lastPrice = None
        # Remember the price of the last purchase.
        self.boughtPrice = None

    def act(self, environment):

        """
        This method is called in every period. From period to period the environment can change

        :param environment:
        :return:
        """

        super(StudentStockTradingAgent, self).act(environment)  # Do not remove this line

        # Expecting the price to move up, hence buy if the price of the previous period is lower than the current one.
        if self.lastPrice < self.price:
            # Get the maximum amout of stocks that can be bought with the available budget.
            maxStocks = self.calculateMaxStocks(self.budget, self.price)

            if maxStocks > 0:
                # Buy the maximum amount of stocks.
                stocksBought = self.buy(maxStocks)

                # Remember the purchase price of the stock.
                self.boughtPrice = self.price

                # Output status.
                outputText = "Bought " + str(stocksBought) + " stocks at " + str(self.boughtPrice) + " for " + str(self.boughtPrice * stocksBought) + "."
                self.memory.append(outputText)
                print(outputText)

        elif self.boughtPrice is not None:
            # Sell the maximum amount of stocks.
            stocksSold = self.sell(self.stock_amount)

            # Get the profit.
            profit = self.calculateProfit(self.boughtPrice, self.price, stocksSold)

            # Output status.
            outputText = "Sold " + str(stocksSold) + " stocks at " + str(self.price) + " for " + str(self.price * stocksSold) + ". Profit is " + str(profit) + "."
            self.memory.append(outputText)
            print(outputText)

            # Reset the purchase price variable.
            self.boughtPrice = None

        # Remember the the current price for use in the next period.
        self.lastPrice = self.price

        #for memory in self.memory:
        #    print(memory)


        # You can access e.g. the current price with 'self.price'. Other available attributes are:
        # 'budget', 'stock_amount', 'current_stock_value', 'current_total_value'.
        # You need not to query the sensors as this is done automatically in every round.

        # There are two functions 'buy' and 'sell' that let you buy and sell stocks at the current price.
        # E.g. 'amount = self.buy(1)' buys one stock. If you try to buy more stocks than you can afford (budget!),
        # the maximum possible amount is bought and returned to you:
        # E.g. price=100, budget=200 => 'amount = self.buy(10)' will buy 2 stocks, 'amount' is set to '2'.
        # The same is true for 'sell', you cannot sell more stocks than you have.

        # If you are interested into details have a look at the parent class of this class (StockTradingAgent) where
        # the above attributes and methods are implemented. You must not change anything there.

        # You need not to access the passed environment object as well as you are only allowed to use the information
        # that are available to the agent described above. In addition you are free to implement any memory, goals, etc.
        # for your agent and work with these information. E.g. you could save all past prices by the command
        # 'self.memory.append(self.price)'. 'self.memory' is a list and is accessible by indexing; 'self.memory[-1]'
        # returns the last price.

        # Insert custom code here...

    def calculateMaxStocks(self, currentBudget, currentStockPrice):

        """
        This method calculates the maximum amout of the given stocks that can be purchased with the given budget.

        :param currentBudget, currentStockPrice:
        :return: maxStocks
        """

        # Calculate max amaount of stocks depending on current budget.
        maxStocks = currentBudget / currentStockPrice

        # Return truncated comma values.
        return int(math.floor(maxStocks))
        pass

    def calculateProfit(self, oldPrice, newPrice, amount):

        """
        This method calculates the profit made by investing cash into a stock.

        :param oldPrice, newPrice, amount:
        :return: profit
        """

        # Calculate total purchase value.
        oldValue = oldPrice * amount

        # Calculate total sell amount.
        newValue = newPrice * amount

        # Return difference.
        return newValue - oldValue
        pass
