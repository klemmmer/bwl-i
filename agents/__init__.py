import importlib
import os

from agent import StockTradingAgent


def get_agents():
    files = os.listdir('agents')
    agents = []

    for file_ in files:
        if file_.startswith('__') or not file_.endswith('.py'):
            # print "Skipped {0}".format(file_)
            continue

        try:
            module = importlib.import_module('.' + file_[:-3], 'agents')
        except ImportError as e:
            print e
            continue

        for name in dir(module):
            cls = getattr(module, name)
            # Check if attribute is a) class b) subclass of Agent c) not Agent itself
            if isinstance(cls, type) and issubclass(cls, StockTradingAgent) and cls != StockTradingAgent:
                agents.append(cls)

    return agents
